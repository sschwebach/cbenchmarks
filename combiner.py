__author__ = 'Sam Schwebach'
__name__ = "__main__"

import sys, getopt, re, os, fnmatch

# This script recursively goes through subdirectories and finds files with the .bes extension and puts them into a single file
# This script is a modified version of a script used for the same purpose in ECE 552 to combine verilog files

fileHeader = "/*\nECE 552 Project 1 - Sam Schwebach and David Tranlam\nI'm guessing that we'll want all our stuff in a single .v file at some point\nsince certain perl scripts won't be re-written any time sooon\n*/\n\n"

def main(argv):
    outputFile = ''
    directory = ''
    try:
        opts, args = getopt.getopt(argv, "ho:od:o",["dir=", "ofile="])
    except getopt.GetoptError:
        print('Usage: combiner.py -d <directory> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('Usage: combiner.py -d <directory> -o <outputfile>')
            sys.exit()
        elif opt in ("-o", "--ofile"):
            outputFile = arg
        elif opt in ("-d", "--dir"):
            directory = arg
    print("Output file is {}".format(outputFile))
    #print(fileHeader)
    out = open(outputFile, "w+")
    #out.write(fileHeader)
    print('Directory is {}'.format(directory))
    walkFiles(directory, out)
    out.close()


def printStuff():
    print("hello world!")


def parseTrace():
    pass

def walkFiles(directory, out):
    print('walk_dir (absolute) = ' + os.path.abspath(directory))
    list_file_path = os.path.join(directory, 'my-directory-list.txt')
    for root, subdirs, files in os.walk(directory):
        print('--\nroot = ' + root)
        for file in files:
            if file.endswith(".bes"):
                print("Added " + file)
                fin = open(os.path.join(root, file))
                for line in fin.readlines():
                    out.write(line)

                fin.close()
                out.write('\n')
    print('list_file_path = ' + list_file_path)


if __name__ == "__main__":
    main(sys.argv[1:])
