adb kill-server
adb push DFT/dft_arm /data/local/tmp
adb push Matrix/serial_matrix_multiply_arm /data/local/tmp
adb push Matrix/serial_matrix_add_arm /data/local/tmp
adb push Ocean/serial_ocean_arm /data/local/tmp
adb push ArrayCopy/arrayCopy_arm /data/local/tmp
adb push Divergence/branchDiv_arm /data/local/tmp
adb push Divergence/memDiv_arm /data/local/tmp
