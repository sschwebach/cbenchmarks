//
// Created by Sam on 5/7/2016.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int arraySize, debug, iterations;

    if (argc != 4) {
        printf("The arguments you entered are wrong.\n");
        printf("./arrayCopy <arraySize> <iterations> <debug>\n");
        return EXIT_FAILURE;
    } else {
        arraySize = atoi(argv[1]);
        iterations = atoi(argv[2]);
        debug = atoi(argv[3]);
    }
    int i = 0;
    for (i = 0; i < iterations; i = i + 1){
        int *originArray = malloc(sizeof(int) * arraySize);
        int *destinationArray = malloc(sizeof(int) * arraySize);
        // initialize the origin array
        int i = 0;
        for (i = 0; i < arraySize; i = i + 1) {
            if (debug) {
                originArray[i] = i;
            } else {
                originArray[i] = rand();
            }
        }

        // array initialization is done, now copy it into the other array
        memcpy(destinationArray, originArray, sizeof(int) * arraySize);

        // our work is done, but we can print out the debug arrays if desired
        if (debug){
            printf("[");
            for (i = 0; i < arraySize; i = i + 1){
                printf("%d, ", destinationArray[i]);
            }
            printf("]\n");
        }
    }


}