cmake_minimum_required(VERSION 3.3)
project(Divergence)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES_MEM MemDivMain.c)
set(SOURCE_FILES_BRANCH BranchDivMain.c)

add_executable(DivergenceBranch ${SOURCE_FILES_BRANCH})
add_executable(DivergenceMem ${SOURCE_FILES_MEM})