#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int iterations, arraySize, debug;

    if (argc != 4){
        printf("The arguments you entered are wrong.\n");
        printf("./memDiv <iterations> <arraySize> <debug>\n");
        return EXIT_FAILURE;
    } else {
        iterations = atoi(argv[1]);
        arraySize = atoi(argv[2]);
        debug = atoi(argv[3]);
    }

    // there are two arrays we need to initialize
    int *sourceArray = malloc(sizeof(int) * arraySize);
    int *resultArray = malloc(sizeof(int) * iterations);

    int i = 0;
    // initiailize the source array
    for (i = 0; i < arraySize; i = i + 1){
        if (debug){
            sourceArray[i] = i;
        } else {
            sourceArray[i] = rand();
        }
    }

    // now copy the data over randomly
    for (i = 0; i < iterations; i = i + 1){
        int location = rand() % arraySize;
        resultArray[i] = sourceArray[location];
        if (debug){
            printf("Result %d picked source array location %d (%d)\n", i, location, sourceArray[location]);
        }
    }

    // now we're done
    if (debug){
        printf("Result array:\n");
        printf("[");
        for (i = 0; i < iterations; i = i + 1){
            printf("%d, ", resultArray[i]);
        }
        printf("]\n");
    }
}