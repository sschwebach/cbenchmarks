#include <stdio.h>
#include <stdlib.h>

// returns a random integer based on the path chosen
// the integer is completely arbitrary to confuse the compiler
int choosePath(int maxBranches){
    int path = rand() % maxBranches;
    switch(path){
        case 0:
            return 0;
            break;
        case 1:
            return 123;
            break;
        case 2:
            return 154;
            break;
        case 3:
            return 103;
            break;
        case 4:
            return 513;
            break;
        case 5:
            return 65189;
            break;
        case 6:
            return -1981;
            break;
        case 7:
            return 5569151;
            break;
        case 8:
            return -661218;
            break;
        case 9:
            return 251;
            break;
        case 10:
            return 21684;
            break;
        case 11:
            return -9991;
            break;
        case 12:
            return 565498;
            break;
        case 13:
            return 1356914;
            break;
        case 14:
            return 64/41;
            break;
        case 15:
            return -15614999;
            break;
        case 16:
            return 54110;
            break;
        case 17:
            return 5;
            break;
        case 18:
            return 9581652;
            break;
        case 19:
            return 98412211178;
            break;
    }
    return -1;
}

int main(int argc, char *argv[]) {
    int iterations, maxBranches, debug;

    if (argc != 4){
        printf("The arguments you entered are wrong.\n");
        printf("./branchDiv <iterations> <maxBranches> <debug>\n");
        return EXIT_FAILURE;
    } else {
        iterations = atoi(argv[1]);
        maxBranches = atoi(argv[2]);
        debug = atoi(argv[3]);
    }

    if (maxBranches > 20){
        printf("Error. Max branches currently supports values up to 20\n");
        return EXIT_FAILURE;
    }

    // the result array we need to initialize
    int *resultArray = malloc(sizeof(int) * iterations);

    int i = 0;

    // now copy the data over randomly
    for (i = 0; i < iterations; i = i + 1){
        int data = choosePath(maxBranches);
        resultArray[i] = data;
        if (debug){
            printf("Result %d picked random path resulting in %d\n", i, data);
        }
    }

    // now we're done
    if (debug){
        printf("Result array:\n");
        printf("[");
        for (i = 0; i < iterations; i = i + 1){
            printf("%d, ", resultArray[i]);
        }
        printf("]\n");
    }
    return EXIT_SUCCESS;
}