#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//
// Created by Sam on 4/26/2016.
//

unsigned long factorial(unsigned long f)
{
    if ( f == 0 )
        return 1;
    return(f * factorial(f - 1));
}

int main(int argc, char *argv[]){

    int timesteps, debug;


    if (argc != 3){
        printf("The arguments you entered are wrong.\n");
        printf("./serialPi <timesteps> (less than 23) <debug>");
        return EXIT_FAILURE;
    } else{
        timesteps = atoi(argv[1]);
        if (timesteps > 22){
            printf("The arguments you entered are wrong.\n");
            printf("./serialPi <timesteps> (less than 23) <debug>");
            return EXIT_FAILURE;
        }
        debug = atoi(argv[2]);
    }

    double piVal = 0;

    // perform the algorithm here
    int i = 0;
    for (i = 0; i < timesteps; i = i  + 1){
        double newVal = (factorial(6 * i) * (13591409 + 545140134 * i)) / (factorial(3 * i) * pow(factorial(i), 3) * pow(-640320, 3 * i));
        piVal += newVal;
    }

    // multiply our sum by our constant
    double constant = 12 / pow(640320, 1.5);

    piVal = piVal * constant;

    // invert our result
    piVal = 1 / piVal;

    if (debug){
        printf("Approximated pi after %d iterations to be %1.50f", timesteps, piVal);
    }


    return EXIT_SUCCESS;
}