Parallel Benchmarks: CUDA/Renderscript will be done in a parallel fashion
Ocean
DFT
Matrix add, multiply
Parallel computation of pi digits using Bailey-Borwein-Plouffe formula? https://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula
 

Serial Benchmarks: Even in CUDA/Renderscript these will be serially done
Chudnovsky formula for calculating pi https://en.wikipedia.org/wiki/Pi#Rapidly_convergent_series
Sieve of Eratosthenes?

