#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//
// Created by Sam on 4/27/2016.
//

/*
   Direct fourier transform
   x and y are the real and imaginary arrays of m points.
   The result is written in-place back into the array.
   It's perfectly acceptible to keep the imaginary array as zero for things such as audio data
   dir =  1 gives forward transform
   dir = -1 gives reverse transform
   Written by Paul Bourke on http://paulbourke.net/miscellaneous/dft/
*/
void DFT(int dir,int m,double *x1,double *y1)
{
    long i,k;
    double arg;
    double cosarg,sinarg;
    double *x2=NULL,*y2=NULL;

    x2 = malloc(m*sizeof(double));
    y2 = malloc(m*sizeof(double));
    if (x2 == NULL || y2 == NULL)
        return;

    for (i=0;i<m;i++) {
        x2[i] = 0;
        y2[i] = 0;
        arg = - dir * 2.0 * 3.141592654 * (double)i / (double)m;
        for (k=0;k<m;k++) {
            cosarg = cos(k * arg);
            sinarg = sin(k * arg);
            x2[i] += (x1[k] * cosarg - y1[k] * sinarg);
            y2[i] += (x1[k] * sinarg + y1[k] * cosarg);
        }
    }

    /* Copy the data back */
    if (dir == 1) {
        for (i=0;i<m;i++) {
            x1[i] = x2[i] / (double)m;
            y1[i] = y2[i] / (double)m;
        }
    } else {
        for (i=0;i<m;i++) {
            x1[i] = x2[i];
            y1[i] = y2[i];
        }
    }

    free(x2);
    free(y2);
    return;
}

static int isPowerOfTwo(int value) {
    return ((value & (value - 1)) == 0) ? 1 : 0;
}

int main(int argc, char *argv[]) {
    int iterations, arraySize, debug;

    if (argc != 4) {
        printf("The arguments you entered are wrong.\n");
        printf("./fft <arrayPower> <iterations> <debug>");
        return EXIT_FAILURE;
    } else {
        arraySize = atoi(argv[1]);
        iterations = atoi(argv[2]);
        debug = atoi(argv[3]);
    }

    if (!isPowerOfTwo(arraySize)) {
        printf("The array size must be a power of two\n");
        return EXIT_FAILURE;
    }

    if (debug){
        printf("Performing fft on an array of size %d\n", arraySize);
    }

    // initialize our arrays
    double *realArray = malloc(sizeof(double) * arraySize);
    double *imagArray = malloc(sizeof(double) * arraySize);

    // Make them random I guess
    int i = 0;
    for (i = 0; i < arraySize; i = i + 1) {
        if (debug) {
            realArray[i] = (double) i + 1;
            imagArray[i] = (double) i + 1;
        } else {
            realArray[i] = ((double) rand() / (double) RAND_MAX);
            imagArray[i] = ((double) rand() / (double) RAND_MAX);
        }
    }

    for (i = 0; i < iterations; i = i + 1){
        // finally perform the fft
        DFT(1, arraySize, realArray, imagArray);
    }


    // print out if in debug mode
    if (debug){
        printf("The final result is:\n");
        printf("[");
        for (i = 0; i < arraySize; i = i + 1){
            printf("(%f, %f), ", realArray[i], imagArray[i]);
        }
        printf("]\n");
    }

    return EXIT_SUCCESS;
}