cmake_minimum_required(VERSION 3.3)
project(Matrix)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES_ADD matrix_main.c matrix_add.c)

set(SOURCE_FILES_MUL matrix_main.c matrix_mul.c)

add_executable(Matrix_Add ${SOURCE_FILES_ADD})
add_executable(Matrix_Mul ${SOURCE_FILES_MUL})