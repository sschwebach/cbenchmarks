#include <stdio.h>
#include <stdlib.h>

extern void matrixWork(int **grid[2], int xdim, int ydim, int timesteps);

void printGrid(int **grid, int xDim, int yDim);

int main(int argc, char *argv[]) {
    int xdim, ydim, timesteps;
    int **grid[2];
    int i, j, t;
    int doRand;


    /********************Get the arguments correctly (start) **************************/
    /*
    Three input Arguments to the program
    1. X Dimension of the grid
    2. Y dimension of the grid
    3. number of timesteps the algorithm is to be performed
    */

    if (argc != 4) {
        printf("The Arguments you entered are wrong.\n");
        printf("./matrix <matrixSize> <timesteps> <random>\n");
        return EXIT_FAILURE;
    } else {
        xdim = atoi(argv[1]);
        timesteps = atoi(argv[2]);
        doRand = atoi(argv[3]);
        ydim = xdim;
    }
    ///////////////////////Get the arguments correctly (end) //////////////////////////


    /*********************create the grid as required (start) ************************/
    /*
    The grid needs to be allocated as per the input arguments and randomly initialized.
    Remember during allocation that we want to guarantee a contiguous block, hence the
    nasty pointer math.

    // TA NOTE: If you dislike this scheme and want to say, allocate a single contiguous
    // 1D array, and do the pointer arithmetic yourself during the iteration, you may do so.

    To test your code for correctness please comment this section of random initialization.
    */
    grid[0] = (int **) malloc(ydim * sizeof(int *));
    grid[1] = (int **) malloc(ydim * sizeof(int *));
    int *temp = (int *) malloc(xdim * ydim * sizeof(int));
    int *other_temp = (int *) malloc(xdim * ydim * sizeof(int));
    // Force xdim to be a multiple of 64 bytes.
    for (i = 0; i < ydim; i++) {
        grid[0][i] = &temp[i * xdim];
        grid[1][i] = &other_temp[i * ydim];
    }
    for (i = 0; i < ydim; i++) {
        for (j = 0; j < xdim; j++) {
            if (doRand) {
                grid[0][i][j] = rand() % 100;
                grid[1][i][j] = rand() % 100;
            } else {
                if (i == 0 || j == 0 || i == ydim - 1 || j == xdim - 1) {
                    grid[0][j][i] = 100;
                    grid[1][j][i] = 100;
                } else {
                    grid[0][j][i] = 50;
                    grid[1][j][i] = 50;
                }
            }

        }
    }
    ///////////////////////create the grid as required (end) //////////////////////////
    if (!doRand){
        printf("Performing matrix operation on a %d by %d grid for %d timesteps\n\n", xdim, ydim, timesteps);

        printGrid(grid[0], xdim, ydim);
        printf("Times/plus \n");
        printGrid(grid[0], xdim, ydim);

        printf("Equals:\n\n");
    }


    matrixWork(grid, xdim, ydim, timesteps);

    if (!doRand){
        printGrid(grid[0], xdim, ydim);
    }



    // Free the memory we allocated for grid
    free(temp);
    free(other_temp);
    free(grid[0]);
    free(grid[1]);

    return EXIT_SUCCESS;
}


/**
 * Prints the grid in a (hopefully) readable format
 */
void printGrid(int **grid, int xDim, int yDim) {
    int i = 0;
    int j = 0;
    printf("[");
    for (i = 0; i < xDim; i = i + 1) {
        for (j = 0; j < yDim; j = j + 1) {
            printf("%d, ", grid[i][j]);
        }
        if (i < (xDim - 1)) {
            printf("\n");
        }
    }
    printf("]\n");
}