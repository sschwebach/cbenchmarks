//
// Created by Sam on 4/25/2016.
//

void matrixWork(int **grid[2], int xdim, int ydim, int timesteps){
    // current timestep
    int i = 0;
    // x dimension
    int j = 0;
    // y dimension
    int k = 0;
    int ** readGrid = grid[1];
    int ** writeGrid = grid[0];

    for (i = 0; i < timesteps; i = i + 1){
        //iterate through each cell for every timestep
        for (j = 0; j < xdim; j = j + 1){
            for (k = 0; k < ydim; k = k + 1){
                writeGrid[j][k] = readGrid[j][k] + readGrid[j][k];
            }
        }
        int ** temp = readGrid;
        readGrid = writeGrid;
        writeGrid = temp;
    }
}