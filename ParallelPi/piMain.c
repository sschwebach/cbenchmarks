#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//
// Created by Sam on 4/26/2016.
//

int calculatePiDigit(int n) {
    int k = 0;
    double value = (int) 4.0 / (8.0 * k + 1.0) - 2.0 / (8.0 * k + 4.0) - 1.0 / (8.0 * k + 5.0) - 1.0 / (8.0 * k + 6.0);
    while (value < 1){
        value *= 10;
    }
    return value;
}

char printHex(int dec) {
    if (dec <= 9) {
        return (char) 0x30 + dec;
    } else {
        return (char) (0x57 + dec);
    }
}

int main(int argc, char *argv[]) {
    int digits, debug;

    if (argc != 3) {
        printf("The arguments you entered are wrong.\n");
        printf("./parallelPi <digits> <debug>");
        return EXIT_FAILURE;
    } else {
        digits = atoi(argv[1]);
        debug = atoi(argv[2]);
    }

    int *piDigits = malloc(sizeof(int) * digits);
    int i = 0;
    for (i = 0; i < digits; i = i + 1) {
        piDigits[i] = calculatePiDigit(i);
    }



    // print out the string in hex
    if (debug) {
        char *piWord = malloc(sizeof(char) * (digits + 2));
        piWord[0] = printHex(piDigits[0]);
        piWord[1] = '.';
        for (i = 1; i < digits; i = i + 1) {
            printf("Adding hex digit %d to the sum\n", piDigits[i]);
            piWord[i + 1] = printHex(piDigits[i]);
        }
        piWord[digits + 1] = (char) 0;
        //printf("Pi approximated to be %s\n", piWord);
        printf("The actual hex value of pi 3.243F6A\n");
    }

    return EXIT_SUCCESS;
}
