#include <stdio.h>
#include <omp.h>
#include "ocean_utils.h"

void ocean(int **grid[2], int xdim, int ydim, int timesteps) {

    /********************************************************
     * algorithm
     *
     * Two grids are passed in through the grid argument.
     * Each grid itself is an int**, which is a pointer to
     * an array of pointers (see the initialization code).
     *
     * Iterate over the grid[0], performing the Ocean
     * algorithm (described in wiki). Write the result to
     * the other grid. Then, swap the grids for the next
     * iteration.
     ******************************************************/

    // this implementation should use OpenMP to more quickly perform the calculation
    // timestep
    int i = 0;
    // x dimension
    int j = 0;
    // y dimension
    int k = 0;
    // looks like grid[0] is the active grid first
    int **readGrid = grid[0];
    // write to the other one
    int **writeGrid = grid[1];
    //printf("Initial grid:\n");
    //printGrid(readGrid, xdim, ydim);
    // do it for a certain number of timesteps
    //printf("Initial Grid:\n");
    //printGrid(readGrid, xdim, ydim);
    for (i = 0; i < timesteps; i = i + 1) {
        // in each timestep iterate through each cell
        // TODO figure out what needs to be in these pragmas
#pragma omp parallel for \
 schedule(static) shared(readGrid, writeGrid, xdim, ydim) \
 private(j, k)
        for (j = 0; j < xdim; j = j + 1) {
            for (k = 0; k < ydim; k = k + 1) {
                // now sum each cell with its surrounding neighbors
                updateGridValue(readGrid, writeGrid, j, k, xdim, ydim);
            }
        }
        // once the timestep is done, we want to swwap the grids
        int **temp = readGrid;
        readGrid = writeGrid;
        writeGrid = temp;
        //printf("Grid after timestep %d:\n", i);
        //printGrid(readGrid, xdim, ydim);
    }

}
