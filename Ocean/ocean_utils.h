//
// Created by sschwebach on 1/27/16.
//

#ifndef HW1_DIST_OCEAN_UTILS_H
#define HW1_DIST_OCEAN_UTILS_H

/**
 * This function actually does the work of updating grid values in our active grid.
 * Sums the values around a grid, taking into account grids that are on the edge.
 * Reads from gridRead and writes to gridWrite
 */
void updateGridValue(int **gridRead, int **gridWrite, int xPos, int yPos, int xdim, int ydim);

/**
 * Prints the grid in a (hopefully) readable format
 */
void printGrid(int ** grid, int xDim, int yDim);

#endif //HW1_DIST_OCEAN_UTILS_H
