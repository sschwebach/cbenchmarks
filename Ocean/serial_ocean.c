#include <stdio.h>
#include "ocean_utils.h"

void ocean (int **grid[2], int xdim, int ydim, int timesteps)
{

    /********************************************************
     * algorithm
     *
     * Two grids are passed in through the grid argument.
     * Each grid itself is an int**, which is a pointer to
     * an array of pointers (see the initialization code).
     *
     * Iterate over the grid[0], performing the Ocean
     * algorithm (described in wiki). Write the result to
     * the other grid. Then, swap the grids for the next
     * iteration.
     ******************************************************/

    // this version should be done in serial
    // our iterators
    // timestep
    int i = 0;
    // x dimension
    int j = 0;
    // y dimension
    int k = 0;
    // looks like grid[0] is the active grid first
    int ** readGrid = grid[0];
    // write to the other one
    int ** writeGrid = grid[1];
    //printf("Initial grid:\n");
    //printGrid(readGrid, xdim, ydim);
    // do it for a certain number of timesteps
    for (i = 0; i < timesteps; i = i + 1){
        // in each timestep iterate through each cell
        // only iterate through inner wall
        for (j = 0; j < ydim; j = j + 1) {
            for (k = 0; k < xdim; k = k + 1) {
                // keep boundary the same
                if (j == 0 || j == ydim - 1 || k == 0 || k == xdim - 1) {
                    writeGrid[j][k] = readGrid[j][k];
                } else {
                    // calculate the sum
                    int sum = readGrid[j][k];

                    // north
                    sum += readGrid[j - 1][k];

                    // east
                    sum += readGrid[j][k + 1];

                    // south
                    sum += readGrid[j + 1][k];

                    // west
                    sum += readGrid[j][k - 1];

                    // now divide by 5 and set to write grid
                    writeGrid[j][k] = sum / 5;
                }
            }
        }

        // once the timestep is done, we want to swwap the grids
        int ** temp = readGrid;
        readGrid = writeGrid;
        writeGrid = temp;
        //printf("Grid after timestep %d:\n", i);
        //printGrid(readGrid, xdim, ydim);
    }

}
