//
// Created by sschwebach on 1/27/16.
//

#include <stdio.h>
#include <assert.h>
#include "ocean_utils.h"

/**
 * This function actually does the work of updating grid values in our active grid.
 * Sums the values around a grid, taking into account grids that are on the edge.
 * Reads from gridRead and writes to gridWrite
 */
void updateGridValue(int **gridRead, int **gridWrite, int xPos, int yPos, int xdim, int ydim){
    // we want to sum around the grid
    int i = -1;
    int j = -1;
    // the current sum for the grid
    int sum = 0;
    // the count of how many values we actuall want to add
    int count = 0;
    for (i = -1; i <= 1; i = i + 1) {
        for (j = -1; j <= 1; j = j + 1) {
            // our pair [i, j] + [xPos, yPos] represents the current grid location
            int x = i + xPos;
            int y = j + yPos;
            // we only want to add the value if it's in the grid
            // and that i and j are in NWSE positions (or is at 0,0)
            if (x >= 0 && x < xdim && y >= 0 && y < ydim
                    && (i == 0 || j == 0 ) ) {
                count++;
                assert(count <= 5);
                sum = sum + gridRead[x][y];
            }
        }
    }

    // we now have the sum, so let's average it and write it
    sum = sum / count;
    gridWrite[xPos][yPos] = sum;
}

/**
 * Prints the grid in a (hopefully) readable format
 */
void printGrid(int ** grid, int xDim, int yDim){
    int i = 0;
    int j = 0;
    printf("[");
    for (i = 0; i < xDim; i = i + 1){
        for (j = 0; j < yDim; j = j + 1){
            printf("%d, ", grid[i][j]);
        }
        if (i < (xDim - 1)){
            printf("\n");
        }
    }
    printf("]\n");
}
